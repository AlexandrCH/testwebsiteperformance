﻿var urlGetAll = "/Home/getAll";

var table = $("#tableResults");
var responseTimeArray = [];
var urlAddressArray = [];
function GetAllData() {

    $.ajax({
        url: urlGetAll,
        type: "GET",
        ContentType: 'application/json',
        datatype: 'json',

        success: function (data, status, xhr) {
            CreateTable(data);
        },

        error: function (xhr, status, error) {
            alert(xhr + "---" + status + "---" + error);
        }
    });
}