﻿function CreateGraph() {
    var ctx = document.getElementById("myChart");
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: urlAddressArray,
            datasets: [{
                label: "Sites response chart by Max values",
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: responseTimeArray
            }]
        },

        // Configuration options go here
        options: {}
    });
}