﻿var urlSendRequest = "/api/sendRequest";

$(function () {
    $('#loading').hide();
    $(function () {
        $("#startButton").click(function () {
            $("#startButton").hide();
            $("#responsResult").text("");
            var enteredUrl = $("#urlInput").val();
            $("#loading").show();
            $.ajax({
                url: urlSendRequest,
                type: "POST",
                data: JSON.stringify(enteredUrl),
                contentType: "application/json",

                success: function (data, status, xhr) {
                    $("#responsResult").append('<div class="alert alert-success">' + "Successfully! Was performed 5 requests for each page from " + enteredUrl + "  sitemap’s. Find Chart and Table with results below" + '</div>');
                    table.text("");
                    GetAllData();
                    $('#loading').hide();
                    $("#startButton").show();
                },
                error: function (xhr, status, error) {
                    $("#responsResult").append('<div class="alert alert-danger">' + xhr.statusText + "   ☺"  + '</div>');
                    $('#loading').hide();
                    $("#startButton").show();
                }
            });
        });
    });
});