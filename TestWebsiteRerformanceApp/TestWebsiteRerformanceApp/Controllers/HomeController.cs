﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Newtonsoft.Json;
using TestWebsiteRerformanceApp.Core.Interfaces;
using TestWebsiteRerformanceApp.Core.Interfaces.Repositories;
using TestWebsiteRerformanceApp.Core.Models;

namespace TestWebsiteRerformanceApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<SiteAddress> _repository;
        public HomeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            this._repository = unitOfWork.Repository;
        }
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        [HttpGet]
        public async Task<ContentResult> GetAll()
        {
            var t = await _repository.GetAll();
            var jsonData = JsonConvert.SerializeObject(t, Newtonsoft.Json.Formatting.None,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            return Content(jsonData, "application/json");
        }
    }
}
