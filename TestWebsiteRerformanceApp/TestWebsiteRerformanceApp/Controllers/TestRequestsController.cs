﻿using System;
using System.Web.Http;
using TestWebsiteRerformanceApp.Core.Interfaces;

namespace TestWebsiteRerformanceApp.Controllers
{
    public class TestRequestsController : ApiController
    {
        private readonly IWebCrawler _webCrawler;
        private readonly IActionsWithData _actionsWithData;

        public TestRequestsController( IWebCrawler webCrawler, IActionsWithData actionsWithData)
        {
            this._webCrawler = webCrawler;
            this._actionsWithData = actionsWithData;
        }

        [HttpPost]
        [Route("api/sendRequest")]
        public IHttpActionResult Post([FromBody]string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return BadRequest();
            }

            try
            {
                var urlsToRequest = _webCrawler.GetAllUrlLinks(value);

                foreach (var item in urlsToRequest)
                {
                    _actionsWithData.CreateRequestSaveData(item);
                }

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
