﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using TestWebsiteRerformanceApp.Core.Interfaces;
using TestWebsiteRerformanceApp.Core.Interfaces.Repositories;
using TestWebsiteRerformanceApp.Core.Models;

namespace TestWebsiteRerformanceApp.Core
{
    public class ActionsWithData : IActionsWithData
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<SiteAddress> _repository;

        public ActionsWithData(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            _repository = unitOfWork.Repository;
        }

        public void CreateRequestSaveData(string value)
        {
            List<string> timeTakenList = new List<string>();
            //create 5 requests
            for (int i = 0; i < 5; i++)
            {
                var request = (HttpWebRequest)WebRequest.Create(value);
                var timer = new Stopwatch();
                timer.Start();
                var response = (HttpWebResponse)request.GetResponse();
                timer.Stop();
                var timeTaken = timer.ElapsedMilliseconds.ToString();
                timeTakenList.Add(timeTaken);
            }

            SaveData(value, timeTakenList);
        }

        private void SaveData(string url, List<string> timeTaken)
        {
            var responses = new List<SiteTimeResponse>();

            foreach (var x in timeTaken)
            {
                responses.Add(new SiteTimeResponse() { Time = x });
            }

            var data = new SiteAddress()
            {
                SiteName = url,
                siteTimeResponses = responses
            };
            _repository.Add(data);
            _unitOfWork.Save();
        }
    }
}
