﻿using System.Collections;
using System.Collections.Generic;

namespace TestWebsiteRerformanceApp.Core.Models
{
    public class SiteAddress : Entity
    {
        public string SiteName { get; set; }
        public virtual ICollection<SiteTimeResponse> siteTimeResponses { get; set; }
    }
}
