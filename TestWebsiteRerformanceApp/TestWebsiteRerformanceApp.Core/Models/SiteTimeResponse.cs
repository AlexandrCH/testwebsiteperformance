﻿namespace TestWebsiteRerformanceApp.Core.Models
{
    public class SiteTimeResponse : Entity
    {
        public string Time { get; set; }
        public virtual SiteAddress siteAddress { get; set; }
    }
}
