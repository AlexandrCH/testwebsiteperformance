﻿using System.Collections.Generic;

namespace TestWebsiteRerformanceApp.Core.Interfaces
{
    public interface IWebCrawler
    {
        List<string> GetAllUrlLinks(string url);
    }
}