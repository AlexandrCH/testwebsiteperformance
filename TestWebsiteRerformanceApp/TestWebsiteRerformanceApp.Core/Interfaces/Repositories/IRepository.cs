﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestWebsiteRerformanceApp.Core.Models;

namespace TestWebsiteRerformanceApp.Core.Interfaces.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        IEnumerable<T> All();
        Task<IEnumerable<T>> GetAll();
        void Add(T entity);
    }
}