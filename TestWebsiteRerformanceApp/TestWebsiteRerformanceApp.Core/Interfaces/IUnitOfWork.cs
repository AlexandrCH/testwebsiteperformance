﻿using TestWebsiteRerformanceApp.Core.Interfaces.Repositories;
using TestWebsiteRerformanceApp.Core.Models;

namespace TestWebsiteRerformanceApp.Core.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<SiteAddress> Repository { get; }

        void Save();
    }
}