﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using TestWebsiteRerformanceApp.Core.Interfaces;

namespace TestWebsiteRerformanceApp.Core
{
    public class WebCrawler : IWebCrawler
    {
        private const string UrlRegex = @"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?";
        public List<string> GetAllUrlLinks(string url)
        {
            var urlOnlyDomain = GetUrlOnlyDomainName(url);
            var myWebRequest = WebRequest.Create(url);
            List<string> filteredLinks;
            using (var myWebResponse = myWebRequest.GetResponse())
            {
                var streamResponse = myWebResponse.GetResponseStream();

                using (var streamReader = new StreamReader(streamResponse ?? throw new InvalidOperationException()))
                {
                    var data = streamReader.ReadToEnd();
                    filteredLinks = GetLinks(data, urlOnlyDomain);
                }
            }
            return filteredLinks;
        }

        /// <summary>
        /// Get all html links from string according to regex expression
        /// </summary>
        /// <param name="content"></param>
        /// <param name="urlOnlyDomain"></param>
        /// <returns></returns>
        private List<string> GetLinks(string content, string urlOnlyDomain)
        {
            var regexLink = new Regex(UrlRegex);

            var allLinks = new List<string>();
            var filteredLinks = new List<string>();
            foreach (var item in regexLink.Matches(content))
            {
                if (!allLinks.Contains(item.ToString()))
                    allLinks.Add(item.ToString());
            }

            foreach (var link in allLinks)
            {
                if (link.Contains(urlOnlyDomain))
                {
                    filteredLinks.Add(link);
                }
            }
            return filteredLinks;
        }

        private string GetUrlOnlyDomainName(string value)
        {
            Uri url = new Uri(value);
            string host = url.Host;
            string scheme = url.Scheme;
            string urlOnlyDomain = String.Concat(scheme, "://", host);
            return urlOnlyDomain;
        }
    }
}
