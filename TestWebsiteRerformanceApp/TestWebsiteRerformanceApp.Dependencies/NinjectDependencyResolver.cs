﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ninject;
using TestWebsiteRerformanceApp.Core;
using TestWebsiteRerformanceApp.Core.Interfaces;
using TestWebsiteRerformanceApp.Core.Interfaces.Repositories;
using TestWebsiteRerformanceApp.Core.Models;
using TestWebsiteRerformanceApp.Data;
using TestWebsiteRerformanceApp.Data.Repositories;

namespace TestWebsiteRerformanceApp.Dependencies
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
        private void AddBindings()
        {
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            kernel.Bind<IRepository<SiteAddress>>().To<Repository<SiteAddress>>();
            kernel.Bind<IWebCrawler>().To<WebCrawler>();
            kernel.Bind<IActionsWithData>().To<ActionsWithData>();
        }
    }
}