﻿using TestWebsiteRerformanceApp.Core.Interfaces;
using TestWebsiteRerformanceApp.Core.Interfaces.Repositories;
using TestWebsiteRerformanceApp.Core.Models;

namespace TestWebsiteRerformanceApp.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext context;
        private IRepository<SiteAddress> genericRepository;
     

        public UnitOfWork(ApplicationContext context, IRepository<SiteAddress> genericRepository)
        {
            this.context = context;
            this.genericRepository = genericRepository;
        }

        public IRepository<SiteAddress> Repository
        {
            get { return this.genericRepository; }
        }

        public void Save()
        {
            this.context.SaveChanges();
        }
    }
}