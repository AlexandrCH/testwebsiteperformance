﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using TestWebsiteRerformanceApp.Core.Interfaces.Repositories;
using TestWebsiteRerformanceApp.Core.Models;

namespace TestWebsiteRerformanceApp.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        private readonly ApplicationContext db;
        private IDbSet<T> dbEntity = null;

        public Repository(ApplicationContext context)
        {
            this.db = context;
            dbEntity = db.Set<T>();
        }

        public void Add(T entity)
        {
            dbEntity.Add(entity);
        }

        public IEnumerable<T> All()
        {
            throw new System.NotImplementedException();
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await dbEntity.ToListAsync();
        }
    }
}