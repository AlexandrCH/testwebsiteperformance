﻿using System.Data.Entity;
using TestWebsiteRerformanceApp.Core.Models;

namespace TestWebsiteRerformanceApp.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext() : base("DefaultConnection") { }

        public DbSet<SiteAddress> SiteAddressDbSet { get; set; }
    }
}   